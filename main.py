from auth.route import router as RouterAuth
from fastapi import FastAPI
from api import RoomRouter,DepartmentRouter,EmployeeRouter,ServiceRouter,BookingRouter,UseServiceRouter,CheckOutRouter,BedRouter,RoomDetailRouter,VatTuRouter,VatTuHongRouter
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
app = FastAPI()
app.include_router(RouterAuth,tags=["Auth"],prefix="/auth")
app.include_router(RoomRouter,tags=['Room'],prefix='/room')
#app.include_router(ManagerRouter,tags=['Manager'],prefix='/manager')
app.include_router(DepartmentRouter,tags=['Department'],prefix='/department')
app.include_router(EmployeeRouter,tags=['Employee'],prefix='/employee')
#app.include_router(GuestRouter,tags=['Guest'],prefix='/guest')
app.include_router(ServiceRouter,tags=['Service'],prefix='/service')
app.include_router(BookingRouter,tags=['Booking'],prefix='/booking')
app.include_router(UseServiceRouter,tags=['Use Service'],prefix='/use-service')
app.include_router(CheckOutRouter,tags=['Check Out'],prefix='/checkout')
app.include_router(BedRouter,tags=['Bed'],prefix='/bed')
app.include_router(RoomDetailRouter,tags=['Room Detail'],prefix='/roomdetail')
app.include_router(VatTuRouter,tags=['Vat Tu'],prefix='/vattu')
app.include_router(VatTuHongRouter,tags=['Vat Tu Hong'],prefix='/vattuhong')


origins = [
    "*",
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

if __name__=='__main__':
    uvicorn.run('main:app',host='0.0.0.0',port=8123,reload = True)
