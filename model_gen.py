# coding: utf-8
from sqlalchemy import Column, Float, ForeignKey, Integer, String, UniqueConstraint
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Bed(Base):
    __tablename__ = 'bed'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    type = Column(String)


class Department(Base):
    __tablename__ = 'department'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)


class Room(Base):
    __tablename__ = 'room'

    id = Column(Integer, primary_key=True)
    price = Column(Float)
    room_number = Column(String)


class Service(Base):
    __tablename__ = 'service'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    price = Column(Float)


class Vattu(Base):
    __tablename__ = 'vattu'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    price = Column(Float)


class Booking(Base):
    __tablename__ = 'booking'

    id = Column(Integer, primary_key=True, index=True)
    roomid = Column(ForeignKey('room.id'))
    guest_name = Column(String)
    guest_phone = Column(String)
    guest_address = Column(String)
    guest_id_card = Column(String)
    checkstatus = Column(String)
    datetime_in = Column(Float)
    datetime_out_intend = Column(Float)

    room = relationship('Room')


class Employee(Base):
    __tablename__ = 'employee'

    id = Column(Integer, primary_key=True, index=True)
    departmentid = Column(ForeignKey('department.id'))
    name = Column(String)
    account = Column(String)
    password = Column(String)
    phone = Column(String)
    date_of_birth = Column(Float)
    address = Column(String)

    department = relationship('Department')


class RoomDetail(Base):
    __tablename__ = 'room_detail'
    __table_args__ = (
        UniqueConstraint('roomid', 'bedid'),
    )

    id = Column(Integer, primary_key=True)
    roomid = Column(ForeignKey('room.id', ondelete='CASCADE'))
    bedid = Column(ForeignKey('bed.id', ondelete='CASCADE'))
    quantity = Column(Integer)

    bed = relationship('Bed')
    room = relationship('Room')


class Checkout(Base):
    __tablename__ = 'checkout'

    id = Column(Integer, primary_key=True)
    bookingid = Column(ForeignKey('booking.id'))
    datetime_in = Column(Float)
    datetime_out = Column(Float)
    stay_days = Column(Float)
    deposit = Column(Float)
    room_rent_money = Column(Float)
    services_money = Column(Float)
    broken_device_money = Column(Float)
    total = Column(Float)

    booking = relationship('Booking')


class Useservice(Base):
    __tablename__ = 'useservice'

    id = Column(Integer, primary_key=True, index=True)
    bookingid = Column(ForeignKey('booking.id'))
    serviceid = Column(ForeignKey('service.id'))
    quantity = Column(Integer)
    total_money = Column(Float)

    booking = relationship('Booking')
    service = relationship('Service')


class VatTuHong(Base):
    __tablename__ = 'vat_tu_hong'

    id = Column(Integer, primary_key=True)
    bookingid = Column(ForeignKey('booking.id'))
    vattuid = Column(ForeignKey('vattu.id'))

    booking = relationship('Booking')
    vattu = relationship('Vattu')
