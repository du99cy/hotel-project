
from . import schema
from ..generic import CRUD
from core.models import database
from sqlalchemy.orm import Session

class CRUD_DEPARTMENT(CRUD):
    pass

crud = CRUD_DEPARTMENT(model=database.Service)

def create_service(db:Session,service:schema.Service):
    return crud.post(db=db,item=service)

def get_all_service(db:Session,page_number:int,number_of_page:int):
    return crud.get_all(db=db,page_number=page_number,number_of_page=number_of_page)

def get_service_via_id(db:Session,service_id:int):
    return crud.get_via_id(db=db,id=service_id)

def delete_service(db:Session,service_id:int):
    return crud.delete(db=db,id=service_id)

def update_service(db:Session,service_id:int,service:schema.Service):
    return crud.put(db=db,id=service_id,item=service)