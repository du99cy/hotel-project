
from . import schema
from ..generic import CRUD
from core.models import database
from sqlalchemy.orm import Session

class CRUD_USESERVICE(CRUD):
    pass

crud = CRUD_USESERVICE(model=database.Useservice)

def create_useservice(db:Session,useservice:schema.UseService):
    return crud.post(db=db,item=useservice)

def get_all_useservice(db:Session,page_number:int,number_of_page:int):
    return crud.get_all(db=db,page_number=page_number,number_of_page=number_of_page)

def get_useservice_via_id(db:Session,useservice_id:int):
    return crud.get_via_id(db=db,id=useservice_id)

def delete_useservice(db:Session,useservice_id:int):
    return crud.delete(db=db,id=useservice_id)

def update_useservice(db:Session,useservice_id:int,useservice:schema.UseService):
    return crud.put(db=db,id=useservice_id,item=useservice)