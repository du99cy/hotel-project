from pydantic import BaseModel,Field
from typing import Optional
from api.service_.schema import Service
class UseService(BaseModel):
    bookingid:Optional[int] = Field(None)
    serviceid:Optional[int] = Field(None)
    quantity:Optional[int] = Field(None)
    total_money:Optional[float] = Field(None)

class UseserviceWithService(BaseModel):
    quantity:Optional[int] = Field(None)
    total_money:Optional[float] = Field(None)
    service:Optional[Service] = Field(None)

    class Config:
        orm_mode = True