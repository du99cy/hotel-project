from fastapi import APIRouter,Depends,Query,Path
import auth
from sqlalchemy.orm import Session
from . import schema
from . import database

router = APIRouter()

@router.post("/")
async def create_useservice(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),useservice:schema.UseService = Depends() ):
    return database.create_useservice(db=db,useservice = useservice)

@router.get('/')
async def get_all_useservice(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),page_number:int = Query(1),number_of_page:int = Query(4)):
    return database.get_all_useservice(db=db,page_number=page_number,number_of_page=number_of_page)

@router.get('/{useservice_id}')
async def get_useservice_via_id(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),useservice_id:int = Path(...)):
    return database.get_useservice_via_id(db=db,useservice_id=useservice_id)

@router.delete('/{useservice_id}')
async def delete_useservice(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),useservice_id:int = Path(...)):
    return database.delete_useservice(db=db,useservice_id=useservice_id)

@router.put('/{useservice_id}')
async def update_useservice(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),useservice_id:int = Path(...),useservice:schema.UseService= Depends()):
    return database.update_service(db=db,useservice_id=useservice_id,useservice=useservice)


