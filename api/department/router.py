from fastapi import APIRouter,Depends,Query,Path
import auth
from sqlalchemy.orm import Session
from . import schema
from . import database

router = APIRouter()

@router.post("/")
async def create_department(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),department:schema.Department = Depends() ):
    return database.create_department(db=db,department=department)

@router.get('/')
async def get_all_department(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),page_number:int = Query(1),number_of_page:int = Query(4)):
    return database.get_all_department(db=db,page_number=page_number,number_of_page=number_of_page)

@router.get('/{department_id}')
async def get_department_via_id(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),department_id:int = Path(...)):
    return database.get_department_via_id(db=db,department_id=department_id)

@router.delete('/{department_id}')
async def delete_department(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),department_id:int = Path(...)):
    return database.delete_department(db=db,department_id=department_id)

@router.put('/{department_id}')
async def update_department(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),department_id:int = Path(...),department:schema.Department = Depends()):
    return database.update_department(db=db,department_id=department_id,department=department)
