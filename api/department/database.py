
from . import schema
from ..generic import CRUD
from core.models import database
from sqlalchemy.orm import Session

class CRUD_DEPARTMENT(CRUD):
    pass

crud = CRUD_DEPARTMENT(model=database.Department)

def create_department(db:Session,department:schema.Department):
    return crud.post(db=db,item=department)

def get_all_department(db:Session,page_number:int,number_of_page:int):
    return crud.get_all(db=db,page_number=page_number,number_of_page=number_of_page)

def get_department_via_id(db:Session,department_id:int):
    return crud.get_via_id(db=db,id=department_id)

def delete_department(db:Session,department_id:int):
    return crud.delete(db=db,id=department_id)

def update_department(db:Session,department_id:int,department:schema.Department):
    return crud.put(db=db,id=department_id,item=department)