from pydantic import BaseModel,Field
from typing import Optional


class Department(BaseModel):
    name:Optional[str]=Field(None)
    