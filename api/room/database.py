
from typing import Generic
from sqlalchemy.sql.elements import not_


from sqlalchemy.sql.type_api import to_instance
from core.models import database
from sqlalchemy.orm import Session 
from .schema import Room as Room_Schema, RoomWithListRoomDetail
from ..generic import CRUD
from sqlalchemy import or_

class CRUD_(CRUD):
    pass

crud = CRUD_(model=database.Room)


def get_all_room(db:Session,number_of_page:int,page_number:int):
    return crud.get_all(db=db,number_of_page=number_of_page,page_number=page_number)
    #return db.query(database.Room).offset(page_number-1).limit(number_of_page).all()

def get_room_via_id(db:Session,room_id:int):
    return crud.get_via_id(db=db,id=room_id)
    #return db.query(database.Room).filter(database.Room.id==room_id).first()

def create_room(db:Session , room:Room_Schema):
    return crud.post(db=db,item=room)
    # room_data = database.Room(**room.dict())
    # db.add(room_data)
    # db.commit()
    # db.refresh(room_data)
    # return room_data

def delete_room(db:Session,room_id:int):
    return crud.delete(db=db,id=room_id)
    # db.query(database.Room).filter(database.Room.id == room_id).delete()
    # db.commit()
    # return room_id

def update_room(db:Session,room_id:int,room:Room_Schema):
    return crud.put(db=db,id=room_id,item=room)
    #room_query = db.query(database.Room).filter(database.Room.id == room_id).one_or_none()

    #fix error:'_sa_instance_state' is an invalid keyword argument for Room
    # room_query_romm_schema=Room_Schema(**room_query.__dict__)

    # for k,v in vars(room).items():
    #     if v is not None:
    #         setattr(room_query,k,v)

    # data_return = vars(room_query).copy()

    # db.commit()

    # return data_return

def available_rooms(db:Session,not_available_rooms:list):
    list_not_available_room = [room['roomid'] for room in not_available_rooms ]
    available_room = db.query(database.Room).filter(not_(database.Room.id.in_(list_not_available_room)) ).all()
    return available_room

# def get_available_room(db:Session):
#         not_available_room = db.query(database.Booking).with_entities(database.Booking.roomid).filter(or_(database.Booking.checkstatus=='Checked', database.Booking.checkstatus=='Not Checking')).all()
        
#         return available_rooms(db = db,not_available_rooms = not_available_room)
    
def get_available_room_ids(db:Session,start_date:float=None,end_date:float=None):
    start_date = float('-inf') if start_date is None else start_date
    end_date = float('inf') if end_date is None else end_date

    not_available_room = db.query(database.Booking).with_entities(database.Booking.roomid,database.Booking.checkstatus).filter(or_(database.Booking.checkstatus=='Checked', database.Booking.checkstatus=='Not Checking')).filter(not_(or_(end_date < database.Booking.datetime_in, database.Booking.datetime_out_intend < start_date))).all()
    return not_available_room

def get_available_room(db:Session,start_date:float=None,end_date:float=None):
    not_available_room_ids = get_available_room_ids(db,start_date,end_date)
    return available_rooms(db=db,not_available_rooms=not_available_room_ids)

def get_not_available_room(db:Session):
    return db.query(database.Booking).filter(database.Booking.checkstatus.in_(['Checked','Not Checking'])).all()
                

    
    
    
    
   









