from pydantic.fields import Field
from core.setting import Base
from pydantic import BaseModel
from enum import Enum
from typing import Optional,List
from ..room_detail.schema import RoomDetailDB
class CheckStatus(str,Enum):
    not_available = 'N/A'
    available = 'Available'

class CleanStatus(str,Enum):
    clean = 'Cleaned'
    not_clean = 'Not'



class Room(BaseModel):
    
    # checkstatus:Optional[CheckStatus] = Field(None)
    # cleanstatus:Optional[CleanStatus] = Field(None)
    price :Optional[float] = Field(None)
    #bedtype :Optional[str]= Field(None)
    room_number :Optional[str] = Field(None)
    

    class Config:
        orm_mode = True

class RoomWithListRoomDetail(Room):
    id:int
    roomdetails:List[RoomDetailDB]
  

    def __init__(self):
        pass
    class Config:
        orm_mode = True
