from api.booking.schema import BookingWithRoom
from fastapi import APIRouter,Depends,Body,Query,Path
import auth 
from sqlalchemy.orm import Session
from . import database 
from .schema import Room,RoomWithListRoomDetail
from typing import List
from typing import Optional
router = APIRouter()


@router.post('/')
async def create_room(db:Session = Depends(auth.get_db),room:Room =Depends() ,current_user:auth.User = Depends(auth.get_current_user)):
    return database.create_room(db=db,room=room)


@router.get('/',response_model=List[RoomWithListRoomDetail])
async def get_all_room(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),page_number:int = Query(...),number_of_page:int =Query(...) ):
    return database.get_all_room(db=db,number_of_page=number_of_page,page_number=page_number)


@router.get('/{room_id}',response_model=RoomWithListRoomDetail)
async def get_room_via_id(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),room_id:int =Path(...) ):
    return database.get_room_via_id(db=db,room_id=room_id)

@router.delete('/{room_id}')
async def delete_room(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),room_id:int = Path(...)):
    return database.delete_room(db=db,room_id=room_id)

@router.put('/{room_id}')
async def update_room(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),room_id:int = Path(...),room:Room=Depends()):
    return database.update_room(db=db,room_id=room_id,room=room)

@router.get('/all_avalable_room/',response_model=List[RoomWithListRoomDetail])
async def get_all_available_room(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),start_date:Optional[float]=Query(None),end_date:Optional[float]=Query(None) ):

    return database.get_available_room(db=db,start_date=start_date,end_date=end_date)

@router.get('/all_not_avalable_room/',response_model=List[BookingWithRoom])
async def get_all_not_available_room(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user)):
    return database.get_not_available_room(db=db)



 