from sqlalchemy.orm import Session, selectin_polymorphic
class CRUD:
    #db:Session
    def __init__(self,model) -> None:
        self.model = model
        
    
    def get_all(self,db:Session,page_number:int,number_of_page:int):
        return db.query(self.model).offset(page_number-1).limit(number_of_page).all()
    
    def get_via_id(self,db:Session,id:int):
        return db.query(self.model).filter(self.model.id == id).first()
    
    def post(self,db:Session,item):
        item_data = self.model(**item.dict())
        db.add(item_data)
        db.commit()
        db.refresh(item_data)
        return item_data
    
    def delete(self,db:Session,id:int):
        db.query(self.model).filter(self.model.id == id).delete()
        db.commit()
        return id
    
    def put(self,db:Session,id,item):
        item_query = db.query(self.model).filter(self.model.id == id).one_or_none()

        for k,v in vars(item).items():
            if v is not None:
                setattr(item_query,k,v)
        
        data_return = vars(item_query).copy()
        db.commit()
        return data_return
    