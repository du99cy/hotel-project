
from . import schema
from ..generic import CRUD
from core.models import database
from sqlalchemy.orm import Session
from auth.helper import get_password_hash,verify_password
class CRUD_DEPARTMENT(CRUD):
    pass

crud = CRUD_DEPARTMENT(model=database.Employee)

def create_employee(db:Session,employee:schema.Employee):
    #password = employee.password
    employee.password = get_password_hash(employee.password)
    # print(password,employee.password)
    # return verify_password(password,employee.password)
    return crud.post(db=db,item=employee)

def get_all_employee(db:Session,page_number:int,number_of_page:int):
    return crud.get_all(db=db,page_number=page_number,number_of_page=number_of_page)

def get_employee_via_id(db:Session,employee_id:int):
    return crud.get_via_id(db=db,id=employee_id)

def delete_employee(db:Session,employee_id:int):
    return crud.delete(db=db,id=employee_id)

def update_employee(db:Session,employee_id:int,employee:schema.Employee):
    employee.password = get_password_hash(employee.password) if employee.password else employee.password
    return crud.put(db=db,id=employee_id,item=employee)