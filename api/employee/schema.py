from pydantic import BaseModel,Field
from typing import Optional


class EmployeeBase(BaseModel):
    
    departmentid :Optional[int] = Field(None)
    name:Optional[str] = Field(None)
    account :Optional[str] = Field(None)
    
    phone:Optional[str] = Field(None)
    date_of_birth :Optional[str] = Field(None)
    address :Optional[str] = Field(None)
    
    

class Employee(EmployeeBase):
    
    password:Optional[str] = Field(None)

class EmployeeReturn(EmployeeBase):
    id:int
    class Config:
        orm_mode = True
   
    




