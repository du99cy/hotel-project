from fastapi import APIRouter,Depends,Query,Path
import auth
from sqlalchemy.orm import Session
from . import schema
from . import database
from typing import List

router = APIRouter()

@router.post("/",response_model=schema.EmployeeReturn)
async def create_employee(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),employee:schema.Employee = Depends() ):
    return database.create_employee(db=db,employee=employee)

@router.get('/',response_model=List[schema.EmployeeReturn])
async def get_all_employee(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),page_number:int = Query(1),number_of_page:int = Query(4)):
    return database.get_all_employee(db=db,page_number=page_number,number_of_page=number_of_page)

@router.get('/{employee_id}',response_model=schema.EmployeeReturn)
async def get_employee_via_id(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),employee_id:int = Path(...)):
    return database.get_employee_via_id(db=db,employee_id=employee_id)

@router.delete('/{employee_id}')
async def delete_employee(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),employee_id:int = Path(...)):
    return database.delete_employee(db=db,employee_id=employee_id)

@router.put('/{employee_id}',response_model=schema.EmployeeReturn)
async def update_employee(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),employee_id:int = Path(...),employee:schema.Employee = Depends()):
    return database.update_employee(db=db,employee_id=employee_id,employee=employee)
