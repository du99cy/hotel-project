from pydantic import BaseModel,Field
from typing import Optional

class VatTu(BaseModel):
    name:Optional[str] = Field(None)
    price:Optional[float] = Field(None)

    class Config:
        orm_mode = True