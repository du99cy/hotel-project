from pydantic import BaseModel,Field
from typing import Optional


class Bed(BaseModel):
    name : Optional[str] = Field(None)
    type : Optional[str] = Field(None)

    class Config:
        orm_mode = True
    