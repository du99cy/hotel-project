
from . import schema
from ..generic import CRUD
from core.models import database
from sqlalchemy.orm import Session

class CRUD_BED(CRUD):
    pass

crud = CRUD_BED(model=database.Bed)

def create_bed(db:Session,bed:schema.Bed):
    return crud.post(db=db,item=bed)

def get_all_bed(db:Session,page_number:int,number_of_page:int):
    return crud.get_all(db=db,page_number=page_number,number_of_page=number_of_page)

def get_bed_via_id(db:Session,bed_id:int):
    return crud.get_via_id(db=db,id=bed_id)

def delete_bed(db:Session,bed_id:int):
    return crud.delete(db=db,id=bed_id)

def update_bed(db:Session,bed_id:int,bed:schema.Bed):
    return crud.put(db=db,id=bed_id,item=bed)