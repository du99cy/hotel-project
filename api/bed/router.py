from fastapi import APIRouter,Depends,Query,Path
import auth
from sqlalchemy.orm import Session
from . import schema
from . import database
from api import bed

router = APIRouter()

@router.post("/")
async def create_bed(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),bed:schema.Bed = Depends() ):
    return database.create_bed(db=db,bed=bed)

@router.get('/')
async def get_all_bed(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),page_number:int = Query(1),number_of_page:int = Query(4)):
    return database.get_all_bed(db=db,page_number=page_number,number_of_page=number_of_page)

@router.get('/{bed_id}')
async def get_bed_via_id(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),bed_id:int = Path(...)):
    return database.get_bed_via_id(db=db,bed_id=bed_id)

@router.delete('/{bed_id}')
async def delete_bed(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),bed_id:int = Path(...)):
    return database.delete_bed(db=db,bed_id=bed_id)

@router.put('/{bed_id}')
async def update_bed(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),bed_id:int = Path(...),bed:schema.Bed = Depends()):
    return database.update_bed(db=db,bed_id=bed_id,bed=bed)

