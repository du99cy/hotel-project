from pydantic import BaseModel,Field
from typing import Optional



class CheckOut(BaseModel):
    bookingid :Optional[int] = Field(None)
    datetime_in :Optional[float]= Field(None)
    datetime_out: Optional[float]= Field(None)
    stay_days :Optional[float]= Field(None)
    deposit : Optional[float]= Field(None)
    room_rent_money : Optional[float]= Field(None)
    services_money : Optional[float]= Field(None)
    broken_device_money : Optional[float]= Field(None)
    total : Optional[float]= Field(None)