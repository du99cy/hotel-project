from fastapi import APIRouter,Depends,Query,Path
import auth
from sqlalchemy.orm import Session
from . import schema
from . import database

router = APIRouter()

@router.post("/")
async def create_checkout(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),checkout:schema.CheckOut = Depends() ):
    return database.create_checkout(db = db,checkout=checkout)

@router.get('/')
async def get_all_checkout(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),page_number:int = Query(1),number_of_page:int = Query(4)):
    return database.get_all_checkout(db=db,page_number=page_number,number_of_page=number_of_page)

@router.get('/{checkout_id}')
async def get_checkout_via_id(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),checkout_id:int = Path(...)):
    return database.get_checkout_via_id_via_id(db=db,checkout_id=checkout_id)


@router.put('/{checkout_id}')
async def update_checkout(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),checkout_id:int = Path(...),checkout:schema.CheckOut = Depends()):
    return database.update_checkout(db=db,checkout_id=checkout_id,checkout=checkout)
