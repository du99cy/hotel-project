
from . import schema
from ..generic import CRUD
from core.models import database
from sqlalchemy.orm import Session

class CRUD_CHECKOUT(CRUD):
    pass

crud = CRUD_CHECKOUT(model=database.Checkout)

def create_checkout(db:Session,checkout:schema.CheckOut):
    booking = db.query(database.Booking).filter(database.Booking.id == checkout.bookingid).one_or_none()
    booking.checkstatus = 'Checkout'
    db.commit()
    return crud.post(db=db,item=checkout)

def get_all_checkout(db:Session,page_number:int,number_of_page:int):
    return crud.get_all(db=db,page_number=page_number,number_of_page=number_of_page)

def get_checkout_via_id(db:Session,checkout_id:int):
    return crud.get_via_id(db=db,id=checkout_id)

def update_checkout(db:Session,checkout_id:int,checkout:schema.CheckOut):
    return crud.put(db=db,id=checkout_id,item=checkout)