from pydantic import BaseModel,Field
from typing import Optional
from ..bed.schema import Bed



class RoomDetail(BaseModel):
    roomid : Optional[int] = Field(None)
    bedid : Optional[int] = Field(None)
    quantity:Optional[int] = Field(None)

    class Config:
        orm_mode = True

class RoomDetailDB(RoomDetail):
    id:int
    bed:Bed

    class Config:
        orm_mode = True

    
    