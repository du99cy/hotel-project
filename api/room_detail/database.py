
from . import schema
from ..generic import CRUD
from core.models import database
from sqlalchemy.orm import Session

from api import room_detail

class CRUD_ROOMDETAIL(CRUD):
    pass

crud = CRUD_ROOMDETAIL(model=database.RoomDetail)

def create_room_detail(db:Session,room_detail:schema.RoomDetail):
    return crud.post(db=db,item=room_detail)

def get_all_room_detail(db:Session,page_number:int,number_of_page:int):
    return crud.get_all(db=db,page_number=page_number,number_of_page=number_of_page)

def get_room_detail_via_id(db:Session,room_detail_id:int):
    return crud.get_via_id(db=db,id=room_detail_id)

def delete_room_detail(db:Session,room_detail_id:int):
    return crud.delete(db=db,id=room_detail_id)

def update_room_detail(db:Session,room_detail_id:int,room_detail:schema.RoomDetail):
    return crud.put(db=db,id=room_detail_id,item=room_detail)