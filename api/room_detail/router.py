from fastapi import APIRouter,Depends,Query,Path
import auth
from sqlalchemy.orm import Session
from . import schema
from . import database
from typing import List



router = APIRouter()

@router.post("/")
async def create_room_detail(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),room_detail:schema.RoomDetail = Depends() ):
    return database.create_room_detail(db=db,room_detail=room_detail)

@router.get('/',response_model=List[schema.RoomDetailDB])
async def get_all_room_detail(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),page_number:int = Query(1),number_of_page:int = Query(4)):
    return database.get_all_room_detail(db=db,page_number=page_number,number_of_page=number_of_page)

@router.get('/{room_detail_id}',response_model=schema.RoomDetailDB)
async def get_room_detail_via_id(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),room_detail_id:int = Path(...)):
    return database.get_room_detail_via_id(db=db,room_detail_id=room_detail_id)

@router.delete('/{room_detail_id}')
async def delete_room_detail(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),room_detail_id:int = Path(...)):
    return database.delete_room_detail(db=db,room_detail_id=room_detail_id)

@router.put('/{room_detail_id}')
async def update_room_detail(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),room_detail_id:int = Path(...),room_detail:schema.RoomDetail = Depends()):
    return database.update_room_detail(db=db,room_detail_id=room_detail_id,room_detail=room_detail)
