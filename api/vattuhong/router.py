from fastapi import APIRouter,Depends,Query,Path
import auth
from sqlalchemy.orm import Session
from . import schema
from . import database
from typing import List
router = APIRouter()

@router.post("/")
async def create_service(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),service:schema.VatTuHong = Depends() ):
    return database.create_vattuhong(db=db,service = service)

@router.get('/',response_model = List[schema.VatTuHongWithVatTu])
async def get_all_service(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),page_number:int = Query(1),number_of_page:int = Query(4)):
    return database.get_all_vattuhong(db=db,page_number=page_number,number_of_page=number_of_page)


@router.delete('/{service_id}')
async def delete_service(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),service_id:int = Path(...)):
    return database.delete_service(db=db,service_id=service_id)
