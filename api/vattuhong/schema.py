from pydantic import BaseModel,Field
from typing import Optional
from ..vattu.schema import VatTu
class VatTuHong(BaseModel):
    bookingid:Optional[int] = Field(None)
    vattuid:Optional[int] = Field(None)
    class Config:
        orm_mode = True

class VatTuHongWithVatTu(VatTuHong):
    vattu:VatTu

    class Config:
        orm_mode = True