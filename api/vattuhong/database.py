
from . import schema
from ..generic import CRUD
from core.models import database
from sqlalchemy.orm import Session

class CRUD_DEPARTMENT(CRUD):
    pass

crud = CRUD_DEPARTMENT(model=database.VatTuHong)

def create_vattuhong(db:Session,service:schema.VatTuHong):
    return crud.post(db=db,item=service)

def get_all_vattuhong(db:Session,page_number:int,number_of_page:int):
    return crud.get_all(db=db,page_number=page_number,number_of_page=number_of_page)

def delete_service(db:Session,service_id:int):
    return crud.delete(db=db,id=service_id)

