from .room import router as RoomRouter

from .department import router as DepartmentRouter
from .employee import router as EmployeeRouter
from .bed import router as BedRouter
from .service_ import router as ServiceRouter
from .booking import router as BookingRouter
from .use_service import router as UseServiceRouter
from .checkout import router as CheckOutRouter
from .room_detail import router as RoomDetailRouter
from .vattu import router as VatTuRouter
from .vattuhong import router as VatTuHongRouter