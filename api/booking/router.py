from api.vattuhong.schema import VatTuHong
from api.vattuhong.database import create_vattuhong
from typing import List,Optional
from api.room.schema import RoomWithListRoomDetail
from fastapi import APIRouter,Depends,Query,Path
import auth
from sqlalchemy.orm import Session
from . import schema
from . import database


router = APIRouter()

@router.post("/")
async def create_booking(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),booking:schema.Booking = Depends() ):
    return database.create_booking(db=db,booking=booking)

@router.get('/')
async def get_all_booking(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),page_number:int = Query(1),number_of_page:int = Query(4)):
    return database.get_all_booking(db=db,page_number=page_number,number_of_page=number_of_page)

@router.get('/{booking_id}')
async def get_booking_via_id(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),booking_id:int = Path(...)):
    return database.get_booking_via_id(db=db,booking_id=booking_id)


@router.put('/{booking_id}')
async def update_booking(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),booking_id:int = Path(...),booking:schema.Booking = Depends()):
    return database.update_booking(db=db,booking_id=booking_id,booking=booking)

@router.get('/checkout/{booking_id}',response_model=schema.Checkout)
async def checkout(db:Session = Depends(auth.get_db),current_user:auth.User = Depends(auth.get_current_user),booking_id:int = Path(...),list_vattuid:Optional[List[int]]=Query([])):
    
    for vt in list_vattuid:
        vth = VatTuHong(bookingid=booking_id,vattuid=vt)
        create_vattuhong(db,vth)
    return database.get_booking_via_id(db,booking_id)
