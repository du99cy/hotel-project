from api.use_service.schema import UseserviceWithService
from api.vattuhong.schema import VatTuHongWithVatTu
from api.room.schema import RoomWithListRoomDetail
from pydantic import BaseModel,Field
from typing import Optional,List
from enum import Enum

class CheckStatus(str,Enum):
    checked = 'Checked'
    not_checking = 'Not Checking'
    cancel = 'Cancel'
    checkout= 'Checkout'

class BookingBase(BaseModel):
    checkstatus:Optional[CheckStatus] = Field(None)
    
    guest_name : Optional[str] = Field(None)
    guest_phone : Optional[str] = Field(None)
    guest_address :  Optional[str] = Field(None)
    guest_id_card : Optional[str] = Field(None)
    datetime_in : Optional[float] = Field(None)
    datetime_out_intend : Optional[float] = Field(None)
class Booking(BookingBase):
    roomid:Optional[int] = Field(None)
    
    

class BookingWithRoom(BookingBase):
    id:int
    room:RoomWithListRoomDetail
    

    class Config:
        orm_mode = True

class Checkout(BookingWithRoom):
    vattuhongs:Optional[List[VatTuHongWithVatTu] ]= Field(None)
    useservices:Optional[List[UseserviceWithService]] = Field(None)
    class Config:
        orm_mode = True