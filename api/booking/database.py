
from . import schema
from ..generic import CRUD
from core.models import database
from sqlalchemy.orm import Session

class CRUD_BOOKING(CRUD):
    pass

crud = CRUD_BOOKING(model=database.Booking)

def create_booking(db:Session,booking:schema.Booking):
    return crud.post(db=db,item=booking)

def get_all_booking(db:Session,page_number:int,number_of_page:int):
    return crud.get_all(db=db,page_number=page_number,number_of_page=number_of_page)

def get_booking_via_id(db:Session,booking_id:int):
    return crud.get_via_id(db=db,id=booking_id)

def update_booking(db:Session,booking_id:int,booking:schema.Booking):
    return crud.put(db=db,id=booking_id,item=booking)

def getroom(db:Session):
    return db.query(database.Booking).all()