from auth.constaint import ACCESS_TOKEN_EXPIRE_MINUTES
from datetime import datetime, timedelta
import json
from typing import Optional
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from sqlalchemy.orm.strategy_options import raiseload
import uvicorn
from fastapi import Depends, FastAPI, HTTPException, status,APIRouter,Query
from sqlalchemy.orm import Session
import core
from .schema import TokenData, User
from .helper import authenticate_user, create_access_token,get_current_user

# to get a string like this run:
# openssl rand -hex 32

def get_db():
    db = core.SessionLocal()
    try:
        yield db
    finally:
        db.close()


router = APIRouter()

@router.post("/token")
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends(),db:Session = Depends(get_db)):
    user_db = authenticate_user(db, form_data.username, form_data.password)
    
    if not user_db:
        raise HTTPException(status_code=404,detail='Not found') 
    user = User(**user_db.__dict__)
    #print(user)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": json.dumps(user.dict())}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer",'user':user}


@router.get("/users/me/")
async def read_users_me(current_user:User = Depends(get_current_user)):
    return current_user


