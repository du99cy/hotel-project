from pydantic import BaseModel
from typing import Optional

class Token(BaseModel):
    access_token: str
    token_type: str





class User(BaseModel):
    id:int
    departmentid :int
    name:str
    account :str

    phone:str
    date_of_birth :str
    address :str

    class Config:
        orm_mode = True
    
    def __str__(self) -> str:
        return f'User(id = {self.id})'
    


class TokenData(BaseModel):
    user:User

class UserInDB(User):
    hashed_password: str