from sqlalchemy import Boolean, Column, ForeignKey, Integer, String,Float,UniqueConstraint
from sqlalchemy.orm import relationship


from ..setting import Base

# class Bed(Base):
#     __tablename__ = 'bed'

#     id = Column(Integer, primary_key=True, index=True)
#     name = Column(String)
#     type = Column(String)

#     roomdetails = relationship('RoomDetail',back_populates='bed') 

# class Room(Base):
#     __tablename__= 'room'

#     id = Column(Integer, primary_key=True, index=True)
#     # checkstatus = Column(String)
#     # cleanstatus = Column(String)
#     price = Column(Float)
#     room_number = Column(String,unique=True)

#     bookings = relationship('Booking',back_populates='room')
#     roomdetails = relationship('RoomDetail',back_populates='room') 

# class RoomDetail(Base):
#     __tablename__ = 'room_detail'

#     id = Column(Integer, primary_key=True, index=True)
#     roomid = Column(Integer,ForeignKey("room.id"))
#     bedid = Column(Integer,ForeignKey('bed.id'))
#     quantity = Column(Integer)

#     room = relationship('Room',back_populates='roomdetails')
#     bed = relationship('Bed',back_populates='roomdetails')

# # class Manager(Base):
# #     __tablename__ = 'manager'

# #     id = Column(Integer, primary_key=True, index=True)
# #     name = Column(String)
# #     salary = Column(Float)
# #     age = Column(Integer)
# #     gender = Column(String)

# #     username = Column(String)
# #     hashed_password = Column(String)

# #     departments = relationship('Department',back_populates='manager')

    

# class Department(Base):
#     __tablename__ = 'department'

#     id = Column(Integer, primary_key=True, index=True)
#     name=Column(String)
#     #managerid = Column(Integer,ForeignKey('manager.id'))

#     #manager = relationship('Manager',back_populates='departments')
#     employees = relationship('Employee',back_populates='department')

# class Employee(Base):
    
#     __tablename__ = 'employee'

#     id = Column(Integer, primary_key=True, index=True)
#     departmentid = Column(Integer,ForeignKey('department.id'))
#     name= Column(String)
#     account = Column(String)
#     password = Column(String)
#     phone=Column(String)
#     date_of_birth = Column(String)
#     address = Column(String)

#     department = relationship('Department',back_populates='employees')

# # class Guest(Base):
# #     __tablename__ = 'guest'

# #     id = Column(Integer, primary_key=True, index=True)
# #     name = Column(String)
# #     gender = Column(String)
    
# #     bookings = relationship('Booking',back_populates='guest')

# class Booking(Base):
#     __tablename__ = 'booking'

#     id = Column(Integer, primary_key=True, index=True)
#     roomid = Column(Integer,ForeignKey('room.id'))
#     guest_name = Column(String)
#     guest_phone = Column(String)
#     guest_address = Column(String)
#     guest_id_card = Column(String)
#     checkstatus = Column(String)
#     datetime_in = Column(Float)
#     datetime_out_intend = Column(Float) 
    

#     room = relationship('Room',back_populates='bookings')
    
#     useservices = relationship('UseService',back_populates='booking')
#     checkout = relationship('CheckOut',back_populates='booking')

# class Service(Base):
#     __tablename__ = 'service'

#     id = Column(Integer, primary_key=True, index=True)
#     name = Column(String)
#     price = Column(Float)

#     useservices = relationship('UseService',back_populates='service')

# class VatTu(Base):
#     __tablename__ = 'vattu'

#     id = Column(Integer, primary_key=True, index=True)
#     name = Column(String)
#     price = Column(Float)

    

# class UseService(Base):
#     __tablename__ = 'useservice'

#     id = Column(Integer, primary_key=True, index=True)
#     bookingid = Column(Integer,ForeignKey('booking.id'))
#     serviceid = Column(Integer,ForeignKey('service.id'))
#     quantity = Column(Integer)
#     total_money = Column(Float)

#     booking = relationship('Booking',back_populates='useservices')
#     service = relationship('Service',back_populates='useservices')

# class CheckOut(Base):
#     __tablename__ = 'checkout'

#     id = Column(Integer, primary_key=True, index=True)
#     bookingid = Column(Integer,ForeignKey('booking.id'))
#     datetime_in = Column(Integer)
#     datetime_out = Column(Integer)
#     stay_days = Column(Integer)
#     deposit = Column(Float)
#     room_rent_money = Column(Float)
#     services_money = Column(Float)
#     total = Column(Float)

#     booking = relationship('Booking',back_populates='checkout')



# # class User(Base):
# #     __tablename__ = "users"

# #     id = Column(Integer, primary_key=True, index=True)
# #     email = Column(String, unique=True, index=True)
# #     hashed_password = Column(String)
# #     is_active = Column(Boolean, default=True)

# #     items = relationship("Item", back_populates="owner")


# # class Item(Base):
# #     __tablename__ = "items"

# #     id = Column(Integer, primary_key=True, index=True)
# #     title = Column(String, index=True)
# #     description = Column(String, index=True)
# #     owner_id = Column(Integer, ForeignKey("users.id"))

# #     owner = relationship("User", back_populates="items")

class Bed(Base):
    __tablename__ = 'bed'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    type = Column(String)
    roomdetails = relationship('RoomDetail',back_populates='bed')

class Department(Base):
    __tablename__ = 'department'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)

    employees = relationship('Employee',back_populates='department')


class Room(Base):
    __tablename__ = 'room'

    id = Column(Integer, primary_key=True)
    price = Column(Float)
    room_number = Column(String)

    bookings = relationship('Booking',back_populates='room')
    roomdetails = relationship('RoomDetail',back_populates='room')

class Service(Base):
    __tablename__ = 'service'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    price = Column(Float)


class Vattu(Base):
    __tablename__ = 'vattu'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    price = Column(Float)

    vattuhongs = relationship('VatTuHong',back_populates='vattu')

class Employee(Base):
    __tablename__ = 'employee'

    id = Column(Integer, primary_key=True, index=True)
    departmentid = Column(ForeignKey('department.id'))
    name = Column(String)
    account = Column(String)
    password = Column(String)
    phone = Column(String)
    date_of_birth = Column(Float)
    address = Column(String)

    department = relationship('Department')


class RoomDetail(Base):
    __tablename__ = 'room_detail'
    __table_args__ = (
        UniqueConstraint('roomid', 'bedid'),
    )

    id = Column(Integer, primary_key=True)
    roomid = Column(ForeignKey('room.id', ondelete='CASCADE'))
    bedid = Column(ForeignKey('bed.id', ondelete='CASCADE'))
    quantity = Column(Integer)

    bed = relationship('Bed')
    room = relationship('Room')


class Checkout(Base):
    __tablename__ = 'checkout'

    id = Column(Integer, primary_key=True)
    bookingid = Column(ForeignKey('booking.id'))
    datetime_in = Column(Float)
    datetime_out = Column(Float)
    stay_days = Column(Float)
    deposit = Column(Float)
    room_rent_money = Column(Float)
    services_money = Column(Float)
    broken_device_money = Column(Float)
    total = Column(Float)

    booking = relationship('Booking')


class Useservice(Base):
    __tablename__ = 'useservice'

    id = Column(Integer, primary_key=True, index=True)
    bookingid = Column(ForeignKey('booking.id'))
    serviceid = Column(ForeignKey('service.id'))
    quantity = Column(Integer)
    total_money = Column(Float)

    booking = relationship('Booking',back_populates='useservices')
    service = relationship('Service')

class VatTuHong(Base):
    __tablename__ = 'vat_tu_hong'

    id = Column(Integer, primary_key=True)
    bookingid = Column(ForeignKey('booking.id'))
    vattuid = Column(ForeignKey('vattu.id'))

    booking = relationship('Booking',back_populates='vattuhongs')
    vattu = relationship('Vattu',back_populates='vattuhongs')

class Booking(Base):
    __tablename__ = 'booking'

    id = Column(Integer, primary_key=True, index=True)
    roomid = Column(ForeignKey('room.id'))
    guest_name = Column(String)
    guest_phone = Column(String)
    guest_address = Column(String)
    guest_id_card = Column(String)
    checkstatus = Column(String)
    datetime_in = Column(Float)
    datetime_out_intend = Column(Float)

    room = relationship('Room')
    useservices = relationship('Useservice',back_populates ='booking')
    vattuhongs = relationship('VatTuHong',back_populates='booking')